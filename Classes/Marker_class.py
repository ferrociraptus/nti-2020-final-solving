# Класс для маркеров
from Classes.Point_class import Point
from collections import namedtuple


class Marker:
    def __init__(self, id: int, center_point: Point, size: float, corners=None, **kwargs):
        self.center_point = center_point
        self.id = id
        self.size = size
        self.corners = corners
        if kwargs.get("name", False):
            self.name = kwargs["name"]


my_tuple = namedtuple("Date", "sec min hour")

a = my_tuple(4,5,6)
b = (8,9,20)


print(b[0])
print(a.sec)